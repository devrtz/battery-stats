battery-stats (0.5.6-3) UNRELEASED; urgency=medium

  * Fix broken battery-stats-graph due to incorrect numpy type of timestamps.

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 23 Aug 2022 09:58:41 +0200

battery-stats (0.5.6-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Remove ancient X-Python-Version field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces
  * d/control: Set Vcs-* to salsa.debian.org

  [ Aurélien COUDERC ]
  * Add patch to migrate to Python 3. (Closes: #936187)
  * Add explicit dependency to lsb-base for init script using
    /lib/lsb/init-functions.
  * Switch to debhelper-compat build dependency, bump compatibility
    level to 12.
  * Add systemd unit in addition to init script.
  * Add patch for supporting XPS 13 power adapter.
  * Bump Standards-Version to 4.5.0:
    - Add Rules-Requires-Root: no.

  [ Petter Reinholdtsen ]
  * Added missing new bc dependency (Closes: #999356)
  * Fixed typo in package description. (Closes: #982599)
  * Implement three level patch naming scheme.
  * Replace patch to add power supplies to more generic
    one (Closes: ##998412).

 -- Petter Reinholdtsen <pere@debian.org>  Sat, 13 Nov 2021 14:00:48 +0100

battery-stats (0.5.6-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Apply patch from Birger Schacht switching to Python 3.
    (Closes: #936187)

 -- Adrian Bunk <bunk@debian.org>  Sun, 07 Feb 2021 13:21:35 +0200

battery-stats (0.5.6-1) unstable; urgency=medium

  * New upstream version 0.5.6.
    - Adjusted udev rules to really trigger on power changes.
    - Adjusted battery-graph.in to avoid syntax error when no discharging
      is logged yet (Closes: #843049).

 -- Petter Reinholdtsen <pere@debian.org>  Thu, 17 Nov 2016 11:51:44 +0100

battery-stats (0.5.5-1) unstable; urgency=medium

  * New upstream version 0.5.5.
    - Avoid error from battery-graph when there is no data yet
      (Closes: #825158).
    - Make collector more robust and avoid blocking hibernation
      when there is no battery available (Closes: #825694).

 -- Petter Reinholdtsen <pere@debian.org>  Tue, 14 Jun 2016 14:17:05 +0200

battery-stats (0.5.4-1) unstable; urgency=medium

  * New upstream version 0.5.4.
    - Dropped fix-desktop-entry.patch, a fix for the same issue is fixed
      upstream.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 22 May 2016 17:41:45 +0200

battery-stats (0.5.3-1) unstable; urgency=medium

  * New upstream version 0.5.3.
    - Now handle hardware with ACAD power source (Closes: #822238).
    - No longer install non-working battery-stats-graph-gnuplot
      (Closes: #819683).
  * Updated Standards-Version from 3.9.7 to 3.9.8.
  * Remove Antonio Radici as uploader.  Thank you for maintaining the
    package for so long.
  * Added new fix-desktop-entry.patch to fix graph drawing when selected
    from desktop menu.

 -- Petter Reinholdtsen <pere@debian.org>  Sun, 22 May 2016 11:19:27 +0200

battery-stats (0.5.2-1) unstable; urgency=medium

  * New upstream version 0.5.2.
    - battery-stats-collector now work with batteries with change_*
      instead of energy_* values (Closes: #819492)
  * Added missing python-matplotlib and libtext-csv-perl
    dependencies (Closes: #819265).

 -- Petter Reinholdtsen <pere@debian.org>  Tue, 29 Mar 2016 20:52:10 +0200

battery-stats (0.5.1-1) unstable; urgency=medium

  * New upstream version 0.5.1.
    - Dropped 01-typo-desktop.patch and 02-reduce-collection-freq.patch
      applied upstream.
    - Now collects data on power status changes (Closes: #819015)

 -- Petter Reinholdtsen <pere@debian.org>  Wed, 23 Mar 2016 09:17:52 +0100

battery-stats (0.5.0-2) unstable; urgency=medium

  * Added cron job to call collect-csv every 10 minutes, as the pm
    power.d hook to call collect-csv when AC power is inserted and
    removed is not working (see bug #818649).
  * Reduce the collection frequence for battery-stats-collector to every
    10 minutes.
  * Change binary architecture list to linux-any from now obsolete sparc,
    ia64, amd64 and i386.

 -- Petter Reinholdtsen <pere@debian.org>  Tue, 22 Mar 2016 14:12:10 +0100

battery-stats (0.5.0-1) unstable; urgency=medium

  * New upstream version 0.5.0.
    - Will log to a new CSV file as well as the original file.
    - Provide new python based graphing tool.
  * Corrected watch file to use new upstream at github.
  * Swiched postrm to use 'set -e' instead of -e in #! line to make
    lintian happy.
  * Switched to more secure vcs links.
  * Implemented optional 'status' option for init.d script.
  * Switched from cdbs to dh.
  * Added 01-typo-desktop.patch to fix typo in new desktop file.

 -- Petter Reinholdtsen <pere@debian.org>  Tue, 22 Mar 2016 08:46:29 +0000

battery-stats (0.4.0-2) unstable; urgency=medium

  * Added missing cmake build dependency.

 -- Petter Reinholdtsen <pere@debian.org>  Mon, 14 Mar 2016 21:46:38 +0100

battery-stats (0.4.0-1) unstable; urgency=medium

  * New upstream release 0.4.0.
    - Collector is reimplemented as a shell script (Closes: #720338).
    - Collector is now able to follow charge level (Closes: #699241).
  * Drop desktop file now included upstream.

 -- Petter Reinholdtsen <pere@debian.org>  Mon, 14 Mar 2016 15:43:53 +0100

battery-stats (0.3.6-2) unstable; urgency=medium

  * Become a co-maintainer.
  * Reorder and reformat dependencies.
  * Switch debhelper from version 7 to 9.
  * Acknowledge NMU (Closes: #817888).

 -- Petter Reinholdtsen <pere@debian.org>  Fri, 11 Mar 2016 22:35:16 +0000

battery-stats (0.3.6-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Dropped 01-skip-non-battery.patch and build-depend on
    libacpi-dev (>= 0.2-4.1) as the workaround is no longer needed.

 -- Petter Reinholdtsen <pere@debian.org>  Fri, 11 Mar 2016 12:16:58 +0100

battery-stats (0.3.6-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Replace homepage URL in control file with a working one. (Closes: #738807)
  * Fix typo in package description (patters->patterns). (Closes: #648294)
  * Add gbp.conf file to enforce use of pristine-tar.
  * Updated Standards-Version from 3.8.3 to 3.9.7.
  * Removed menu file, only need desktop file.
  * Added code to source /lib/lsb/init-functions in init.d script to
    work well with systemd.
  * Drop obsolete DM-Upload-Allowed field from control file.
  * Added Vcs-* links to control file.
  * Changed build dependencies to allow building with gnuplot-qt.
  * Switch to source format 3.0 (quilt).
  * Added 01-skip-non-battery.patch to skip AC "battery". (Closes: 574678)

 -- Petter Reinholdtsen <pere@debian.org>  Fri, 11 Mar 2016 08:49:58 +0000

battery-stats (0.3.6-1) unstable; urgency=low

  * New upstream release that integrates a patch to specify the battery
    number (Closes: 535815)
  * debian/control:
    + Standars-Version bumped to 3.8.3
    + Architecture modified so it builds only where libacpi is present
  * debian/battery-stats.desktop: added a Comment (Closes: 537946)

 -- Antonio Radici <antonio@dyne.org>  Sat, 19 Sep 2009 22:28:40 +0100

battery-stats (0.3.5-1) unstable; urgency=low

  * New upstream version
  * debian/control:
    + Standards-Version bumped to 3.8.1
    + added DM-Upload-Allowed: yes
    + added the new upstream homepage
  * debian/rules: added $(CURDIR) to all occurrences
  * debian/watch: updated with the new path for downloads

 -- Antonio Radici <antonio@dyne.org>  Sat, 13 Jun 2009 20:58:26 +0100

battery-stats (0.3.4-2) experimental; urgency=low

  * readded debian/logrotate which got lost between 0.3.3 and 0.3.4

 -- Antonio Radici <antonio@dyne.org>  Sat, 31 Jan 2009 23:39:09 +0000

battery-stats (0.3.4-1) experimental; urgency=low

  [ Antonio Radici ]
  * debianizing new upstream release which includes:
    + all debian patches
    + autotools packaging and optinal ACPI support
  * debian/ rebuilt to use cdbs, so it will correctly use autotools
  * debian/control:
    + added [i386 amd64 ia64] to libacpi-dev Build-Depends
      (Closes: #512701)
    + added autotools-dev, cdbs and gnuplot-nox as dependencies
  * debian/watch: added the new repository
  * debian/copyright
    + added new upstream author and repository URL
    + GPL renamed GPL-2
  * debian/changelog
    + uploading on experimental
  * debian/battery-stats.desktop
    + added and distributed through debian/control

  [ Gonéri Le Bouder ]
  * debian menu want xpm icon, adjust the debian/rules to generate it
    from the .png and add a builddeps on imagemagick

 -- Antonio Radici <antonio@dyne.org>  Sun, 25 Jan 2009 12:36:18 +0000

battery-stats (0.3.3-3) unstable; urgency=low

  * New maintainer (Closes: #473820)
  * debian/copyright:
    + updated the URL where the source can be found (Closes: #409825)
  * debian/rules:
    + modified to support dpatch
  * debian/patches/gnuplot-newsyntax.patch:
    + use the right gnuplot syntax to generate the graph
      (Closes: #283984)
  * debian/patches/manpages.patch:
    + make the package lintian clean by fixing two manpage warnings
  * debian/patches/acpi-support.patch:
    + add ACPI support (patch from "Michael Bunk" <michael.bunk@gmail.com>)
    + (Closes: #287230)
  * debian/patches/compiler-warnings.patch:
    + remove some compiler warnings

 -- Antonio Radici <antonio@dyne.org>  Sat, 17 Jan 2009 13:37:05 +0000

battery-stats (0.3.3-2) unstable; urgency=low

  * QA upload.
  * Changed maintainer to Debian QA Group
  * Changed to debhelper level 7
  * Updated standards version
  * Clean up rules, copyright, menu
  * Remove log files on purge (closes: #326266)

 -- Peter Eisentraut <petere@debian.org>  Mon, 16 Jun 2008 23:32:04 +0200

battery-stats (0.3.3-1.2) unstable; urgency=low

  * Non-maintainer upload.
  * Added LSB formatted dependency info in init.d script (closes: #468299)

 -- Peter Eisentraut <petere@debian.org>  Tue, 01 Apr 2008 22:45:11 +0200

battery-stats (0.3.3-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * debian/logrotate now use invoke-rc.d (Closes: #367745)

 -- Philippe Le Brouster <plb@ac-grenoble.fr>  Fri, 28 Sep 2007 23:59:00 +0200

battery-stats (0.3.3-1) unstable; urgency=low

  * New upstream release (Closes: bug#185623). Thanks to David Z Maze.

 -- Karl E. Jorgensen <karl@jorgensen.com>  Fri, 21 Mar 2003 01:17:58 +0000

battery-stats (0.3.2-1) unstable; urgency=low

  * package adopted and uploaded by Werner Heuser <wehe@debian.org>
    (Closes: bug#171112)
  * No longer a debian-native package; gives more freedom to sponsor and NMUs.
  * Calling battery-graph from the menu is now immune to log rotation.
    oops.
  * Increased standards-version to 3.5.6.1

 -- Karl E. Jorgensen <karl@jorgensen.com>  Fri, 21 Feb 2003 14:46:38 +0000

battery-stats (0.3.1) unstable; urgency=low

  * Makefile no longer relies on PATH containing "." when calling
    mkinstalldirs. [Credit to Werner Heuser for spotting this]
  * debian/control::Architecture changed to "any". Although this is
    completely untested on other architectures than i386, at least this
    will give non-i386 users a chance to test it.

 -- Karl E. Jorgensen <karl@jorgensen.com>  Thu, 16 Jan 2003 14:20:34 +0000

battery-stats (0.3) unstable; urgency=low

  * The "I Really Hate Trains" release. Despite this, no railway staff was
    injured during the preparation of this release.
  * New program: battery-log to separate out extraction the log data from the
    log files
  * new option --flush on battery-stats-collector to control how often
    the logfile is fflush()'d. It tended to keep the disk spinning, and thus
    wasting battery. oops.
  * battery-stats-collector's args can now be overridden by
    /etc/battery-stats.conf
  * battery-graph now interprets all dates as LOCAL TIME, and not UTC.
    All this UTC stuff was far too confusing.
    This will make the battery-graph command INCOMPATIBLE with (but much more
    intuitive than) the previous version.
  * Dates & Times on the graph are now in local time
  * battery-graph supports durations in units of weeks
  * New options on battery-graph :
     --since  as shorthand for --from xxx --to now
     --display
     --geometry
     --title
  * battery-graph: -T option discouraged: use --text instead.
  * Minor tweaks in the man-page for battery-graph
  * battery-stats-collector: new option: --ignore-missing-battery
  * Split out default settings, /etc/battery-stats.conf is more friendly
    now
  * Changelog for 0.2 incorrectly said that the BIOS estimate was shown on
    the graph. Don't believe it. So far, it is only logged. We apologise for
    any inconvenience this may have caused (as they say on the trains...). No:
    you don't get a refund.

 -- Karl E. Jorgensen <karl@jorgensen.com>  Wed, 27 Nov 2002 14:00:05 +0000

battery-stats (0.2) unstable; urgency=low

  * Can now create graphs
  * Also captures minutes-left according to apm
  * Date is now also written in a human/gnuplot-friendly format
  * Errors can now be logged to syslog
  * Now accessible through the menu : Apps/System
  * Logrotate will now take care of rotating the logs
  * Graph now also shows BIOS estimate

 -- Karl E. Jorgensen <karl@jorgensen.com>  Wed, 18 Sep 2002 23:40:03 +0100

battery-stats (0.1) unstable; urgency=low

  * Initial Release.

 -- Karl E. Jorgensen <karl@jorgensen.com>  Wed, 18 Sep 2002 00:27:36 +0100

Local variables:
mode: debian-changelog
End:
